<?php
/**
 * @package Ef2
 */
/*
Plugin Name: EF2 Vitals
Plugin URI: https://ef2.nl/
Description: Provides a custom API endpoint for getting wp vital information /wp-json/ef2/v1/vitals?token=API_KEY
Version: 1.0.0
Author: EF2 - RV
License: GPLv2 or later
Text Domain: ef2_custom
*/

/*
 * Creating Database table with API key
 * */
register_activation_hook(__FILE__, 'ef2_vitals_install');

function ef2_vitals_install()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "ef2_vitals";

    ef2_vitals_create_table($table_name, $wpdb);

    ef2_vitals_fill_table($table_name, $wpdb);
}

function ef2_vitals_create_table($table_name, $wpdb)
{
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            api_key text NOT NULL,
            PRIMARY KEY  (id)
            ) $charset_collate;";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    dbDelta($sql);
}

function ef2_vitals_fill_table($table_name, $wpdb)
{
    $apiKey = ef2_vitals_generate_api_key();

    $wpdb->insert(
        $table_name,
        [
            'api_key' => $apiKey,
        ]
    );
}

function ef2_vitals_generate_api_key(): string
{
    return md5(uniqid(rand(), true));
}

/*
 * Create Admin Page
 * */
add_action('admin_menu', function () {
    add_menu_page('EF2 Vitals', 'EF2 Vitals', 'manage_options', 'ef2-vitals', 'ef2_vitals_page');
});

function ef2_vitals_page()
{
    echo "<h1>EF2 Vitals</h1>";
    echo "<p>API Key: <strong>" . ef2_vitals_get_api_key() . "</strong></p>";
}

function ef2_vitals_get_api_key(): string
{
    global $wpdb;
    $table_name = $wpdb->prefix . "ef2_vitals";

    $results = $wpdb->get_results("SELECT * FROM $table_name");

    return $results[0]->api_key;
}

/*
 * REST Link for getting version data
 * */
require_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (!function_exists('plugins_api')) {
    require_once(ABSPATH . 'wp-admin/includes/plugin-install.php');
}

add_action('rest_api_init', function () {
    register_rest_route('ef2/v1', '/vitals', [
        'methods'             => 'GET',
        'callback'            => 'rest_vital_func',
        'permission_callback' => '__return_true',
        'args'                => [
            'token' => [
                'default'           => FALSE,
                'validate_callback' => 'ef2_vitals_validate_token',
            ],
        ],
    ]);
});

function ef2_vitals_validate_token($param, $request, $key): bool
{
    $apiKey = ef2_vitals_get_api_key();

    return ($apiKey === $param);
}

function rest_vital_func(WP_REST_Request $request): array
{
    global $wp_version;

    $plugin_data = ef2_get_plugin_data();

    if (empty($plugin_data)) {
        return [
            'wp_core' => [
                'installed' => $wp_version,
            ],
        ];
    }

    return [
        'wp_core' => [
            'installed' => $wp_version,
        ],
        'plugins' => $plugin_data,
    ];
}

function ef2_get_plugin_data(): array
{
    $plugins = [];

    foreach (get_plugins() as $plugin_id => $plugin) {
        $latestAvailableVersion = ef2_get_latest_plugin_version_available($plugin_id);

        if ($latestAvailableVersion === $plugin['Version'] or empty($latestAvailableVersion)) {
            continue;
        }

        $plugins[$plugin_id] = [
            'name'              => $plugin['Name'],
            'installed'         => $plugin['Version'],
            'available_version' => $latestAvailableVersion,
        ];
    }

    return $plugins;
}

function ef2_get_latest_plugin_version_available($plugin_id): string
{
    $plugin_id = explode('/', $plugin_id)[0];

    $args = [
        'slug'   => $plugin_id,
        'fields' => [
            'version' => true,
        ],
    ];

    $call_api = plugins_api('plugin_information', $args);
    if (is_wp_error($call_api)) {
        $api_error = $call_api->get_error_message();

        return '';
    }

    if (empty($call_api->version)) {
        return '';
    }

    return $call_api->version;
}